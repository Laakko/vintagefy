# **Vintagefy**

*(Work in progress)*

An audio VST-plugin created using C++ Juce framework, featuring vintage style effects.

Features: 
- **Saturation** Emulating the soft and warm distortion caused by analog tape machines
- **Delay** Basic delay, with some tape delay characteristics
- **3-point EQ** Bass, Mid, Treble
- **Vibrato effect** Basic vibrato effect for guitar
- **Vinyl effects** Emulating cracks and hisses of a vinyl player

Screenshot:  
![sc1](screenshot1.PNG)