/*
  ==============================================================================

    This file was auto-generated!

    It contains the basic framework code for a JUCE plugin processor.

  ==============================================================================
*/

#pragma once

#include "../JuceLibraryCode/JuceHeader.h"
#include <cmath>
#define PI 3.14159265358979323846
#define TAU 6.28318530717958647692

//==============================================================================
/**
*/
class Vasittu1AudioProcessor  : public AudioProcessor
{
public:
    //==============================================================================
    Vasittu1AudioProcessor();
    ~Vasittu1AudioProcessor();

    //==============================================================================
    void prepareToPlay (double sampleRate, int samplesPerBlock) override;
    void releaseResources() override;

   #ifndef JucePlugin_PreferredChannelConfigurations
    bool isBusesLayoutSupported (const BusesLayout& layouts) const override;
   #endif

    void processBlock (AudioSampleBuffer&, MidiBuffer&) override;

    //==============================================================================
    AudioProcessorEditor* createEditor() override;
    bool hasEditor() const override;

    //==============================================================================
    const String getName() const override;

    bool acceptsMidi() const override;
    bool producesMidi() const override;
    bool isMidiEffect () const override;
    double getTailLengthSeconds() const override;

    //==============================================================================
    int getNumPrograms() override;
    int getCurrentProgram() override;
    void setCurrentProgram (int index) override;
    const String getProgramName (int index) override;
    void changeProgramName (int index, const String& newName) override;

    //==============================================================================
    void getStateInformation (MemoryBlock& destData) override;
    void setStateInformation (const void* data, int sizeInBytes) override;

	// Saturation function
	float Vasittu1AudioProcessor::processDistortion(float sample, double g, double Q, double dist, double fs, int channel);
	float Vasittu1AudioProcessor::processDistortion2(float sample, int channel);


	// Filters
	OwnedArray<IIRFilter> low_filters;
	OwnedArray<IIRFilter> mid_filters;
	OwnedArray<IIRFilter> high_filters;

	OwnedArray<IIRFilter> saturation_lows;
	OwnedArray<IIRFilter> saturation_mids;
	OwnedArray<IIRFilter> saturation_highs;

	// User modifiable parameters
	double Gain_bass, Gain_mid, Gain_treble;
	double Output_Gain;
	double Saturation_threshold, Saturation_gain;
	bool EQ, Effect;
	double rate, depth;
	double delay_len;
	double feedback;

	

	

private:
    //==============================================================================
	JUCE_DECLARE_NON_COPYABLE_WITH_LEAK_DETECTOR(Vasittu1AudioProcessor)

		double Fc_low, Fc_mid, Fc_high;
		double Q_low, Q_mid, Q_high;
		float input, output = 0.f;
		float Mix = 0.9;
		
		Array<float> q;
		//double q;
		Array<float> z;


		// Vibrato
		double deltaAngle, LFOfreq;
		int writeIndex[2] = { 0,0 };
		float phase[2] = { 0,0 };
		int sineindex = 0;
		float vBuffer[2][192001];
		int lenBuffer = 192001;
		/*
		AudioParameterFloat* rate;
		AudioParameterFloat* depth;
		*/
		




};
