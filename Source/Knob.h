/*
  ==============================================================================

    Knob.h
    Created: 4 Mar 2018 1:53:42pm
    Author:  Jukka

  ==============================================================================
*/

#pragma once
#include "../JuceLibraryCode/JuceHeader.h"

class AltLookAndFeel : public LookAndFeel_V4
{
public:
	AltLookAndFeel()
	{
		setColour(Slider::thumbColourId, Colours::red);
	}

	void drawRotarySlider(Graphics& g, int x, int y, int width, int height, float sliderPos,
		const float rotaryStartAngle, const float rotaryEndAngle, Slider& slider) override
	{
		const float radius = jmin(width / 2, height / 2) - 15.0f;
		const float centreX = x + width * 0.5f;
		const float centreY = y + height * 0.5f;
		const float rx = centreX - radius;
		const float ry = centreY - radius;
		const float rw = radius * 2.0f;
		const float angle = rotaryStartAngle + sliderPos * (rotaryEndAngle - rotaryStartAngle);

		// fill
		g.setColour(Colours::black);
		g.fillEllipse(rx, ry, rw, rw);
		
		

		// glow
		g.setColour(Colours::darkred);
		g.drawEllipse(rx, ry, rw, rw, 5.0f);
		g.setOpacity(0.2);

		// outline
		g.setColour(Colours::red);
		g.drawEllipse(rx, ry, rw, rw, 3.0f);

		g.setColour(Colours::white);
		g.drawEllipse(rx, ry, rw, rw, 1.0f);



		const float pointerLength = radius * 0.5f;
		// Pointer outline
		Path p;
		const float pointerThickness = 3.0f;
		p.addRectangle(-pointerThickness * 0.5f, -radius, pointerThickness, pointerLength);
		p.applyTransform(AffineTransform::rotation(angle).translated(centreX, centreY));
		g.setColour(Colours::red);
		g.fillPath(p);
	
		// Pointer inline
		Path p2;
		const float pointerThickness2 = 1.0f;
		p2.addRectangle(-pointerThickness2 * 0.5f, -radius, pointerThickness2, pointerLength);
		p2.applyTransform(AffineTransform::rotation(angle).translated(centreX, centreY));
		g.setColour(Colours::white);
		g.fillPath(p2);



	}
};
