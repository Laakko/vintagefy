/*
  ==============================================================================

  This is an automatically generated GUI class created by the Projucer!

  Be careful when adding custom code to these files, as only the code within
  the "//[xyz]" and "//[/xyz]" sections will be retained when the file is loaded
  and re-saved.

  Created with Projucer version: 5.2.0

  ------------------------------------------------------------------------------

  The Projucer is part of the JUCE library - "Jules' Utility Class Extensions"
  Copyright (c) 2015 - ROLI Ltd.

  ==============================================================================
*/

#pragma once

//[Headers]     -- You can add your own extra header files here --
#include "../JuceLibraryCode/JuceHeader.h"
#include "PluginProcessor.h"
#include "Knob.h"
//[/Headers]



//==============================================================================
/**
                                                                    //[Comments]
    An auto-generated component, created by the Projucer.

    Describe your class and how it works here!
                                                                    //[/Comments]
*/
class PluginEditor  : public AudioProcessorEditor,
                      public Slider::Listener,
                      public Button::Listener,
                      public ComboBox::Listener
{
public:
    //==============================================================================
    PluginEditor (Vasittu1AudioProcessor& p);
    ~PluginEditor();

    //==============================================================================
    //[UserMethods]     -- You can add your own custom methods in this section.
    //[/UserMethods]

    void paint (Graphics& g) override;
    void resized() override;
    void sliderValueChanged (Slider* sliderThatWasMoved) override;
    void buttonClicked (Button* buttonThatWasClicked) override;
    void comboBoxChanged (ComboBox* comboBoxThatHasChanged) override;

    // Binary resources:
    static const char* arcaMajora3bold_otf;
    static const int arcaMajora3bold_otfSize;
    static const char* logo_png;
    static const int logo_pngSize;
    static const char* knob_jpeg;
    static const int knob_jpegSize;
    static const char* rvn_jpg;
    static const int rvn_jpgSize;
    static const char* logo2_png;
    static const int logo2_pngSize;
    static const char* knob2_png;
    static const int knob2_pngSize;
    static const char* glow_png;
    static const int glow_pngSize;


private:
    //[UserVariables]   -- You can add your own custom variables in this section.
    Vasittu1AudioProcessor& processor;
	AltLookAndFeel altlook;
    //[/UserVariables]

    //==============================================================================
    ScopedPointer<Slider> sTre;
    ScopedPointer<Slider> sBass;
    ScopedPointer<Slider> sMid;
    ScopedPointer<Slider> sGainO;
    ScopedPointer<ToggleButton> toggleButton;
    ScopedPointer<Label> label;
    ScopedPointer<ComboBox> comboBox;
    ScopedPointer<ToggleButton> toggleButton2;
    ScopedPointer<Slider> slider3;
    ScopedPointer<Label> label3;
    ScopedPointer<Label> label2;
    ScopedPointer<Label> label5;
    ScopedPointer<Slider> sSaturation;
    ScopedPointer<Slider> sThreshold;
    ScopedPointer<Slider> sRate;
    ScopedPointer<Slider> slider;
    ScopedPointer<Slider> slider2;
    ScopedPointer<Label> label6;
    ScopedPointer<ToggleButton> toggleButton3;
    ScopedPointer<ToggleButton> toggleButton4;
    ScopedPointer<Label> label7;
    ScopedPointer<Slider> sGainO2;
    Image cachedImage_glow_png_1;
    Image cachedImage_logo2_png_2;


    //==============================================================================
    JUCE_DECLARE_NON_COPYABLE_WITH_LEAK_DETECTOR (PluginEditor)
};

//[EndFile] You can add extra defines here...
//[/EndFile]
