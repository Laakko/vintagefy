/*
  ==============================================================================

    This file was auto-generated!

    It contains the basic framework code for a JUCE plugin processor.

  ==============================================================================
*/

#include "PluginProcessor.h"
#include "PluginEditor.h"


//==============================================================================
Vasittu1AudioProcessor::Vasittu1AudioProcessor()
#ifndef JucePlugin_PreferredChannelConfigurations
     : AudioProcessor (BusesProperties()
                     #if ! JucePlugin_IsMidiEffect
                      #if ! JucePlugin_IsSynth
                       .withInput  ("Input",  AudioChannelSet::stereo(), true)
                      #endif
                       .withOutput ("Output", AudioChannelSet::stereo(), true)
                     #endif
                       )
#endif
{

	
	// Initialize Vibrato buffer
	for (int i = 0; i < 2; ++i) {
		for (int j = 0;  j < 192001; ++j) {
			vBuffer[i][j] = 0.0f;
		}
	}

	// Initialize variables

	// Q-values for EQ shelving filters
	Q_high = 2;  
	Q_mid = 2;
	Q_low = 2;
	// Frequency points for EQ shelving filters
	Fc_low = 200;  
	Fc_mid = 2500;
	Fc_high = 6000;


}

Vasittu1AudioProcessor::~Vasittu1AudioProcessor()
{
}

//==============================================================================
const String Vasittu1AudioProcessor::getName() const
{
    return JucePlugin_Name;
}

bool Vasittu1AudioProcessor::acceptsMidi() const
{
   #if JucePlugin_WantsMidiInput
    return true;
   #else
    return false;
   #endif
}

bool Vasittu1AudioProcessor::producesMidi() const
{
   #if JucePlugin_ProducesMidiOutput
    return true;
   #else
    return false;
   #endif
}

bool Vasittu1AudioProcessor::isMidiEffect() const
{
   #if JucePlugin_IsMidiEffect
    return true;
   #else
    return false;
   #endif
}

double Vasittu1AudioProcessor::getTailLengthSeconds() const
{
    return 0.0;
}

int Vasittu1AudioProcessor::getNumPrograms()
{
    return 1;   // NB: some hosts don't cope very well if you tell them there are 0 programs,
                // so this should be at least 1, even if you're not really implementing programs.
}

int Vasittu1AudioProcessor::getCurrentProgram()
{
    return 0;
}

void Vasittu1AudioProcessor::setCurrentProgram (int index)
{
}

const String Vasittu1AudioProcessor::getProgramName (int index)
{
    return {};
}

void Vasittu1AudioProcessor::changeProgramName (int index, const String& newName)
{
}

//==============================================================================
void Vasittu1AudioProcessor::prepareToPlay (double sampleRate, int samplesPerBlock)
{
   
	// Initialize variables
	Output_Gain = 1;
	Saturation_gain = 0.01;
	Saturation_threshold = 0.01;
	rate = 0;
	depth = 0.0001;
	EQ = false;
	Effect = false;

	
	// Initialize filters
	saturation_lows.clear();
	for (int i = 0; i < getTotalNumInputChannels(); ++i) {
		IIRFilter* filter;
		saturation_lows.add(filter = new IIRFilter());
	}

	saturation_mids.clear();
	for (int i = 0; i < getTotalNumInputChannels(); ++i) {
		IIRFilter* filter;
		saturation_mids.add(filter = new IIRFilter());
	}

	saturation_highs.clear();
	for (int i = 0; i < getTotalNumInputChannels(); ++i) {
		IIRFilter* filter;
		saturation_highs.add(filter = new IIRFilter());
	}

	high_filters.clear();
	for (int i = 0; i < getTotalNumInputChannels(); ++i) {
		IIRFilter* filter;
		high_filters.add(filter = new IIRFilter());
	}

	mid_filters.clear();
	for (int i = 0; i < getTotalNumInputChannels(); ++i) {
		IIRFilter* filter;
		mid_filters.add(filter = new IIRFilter());
	}

	low_filters.clear();
	for (int i = 0; i < getTotalNumInputChannels(); ++i) {
		IIRFilter* filter;
		low_filters.add(filter = new IIRFilter());
	}

	// Vibrato
	LFOfreq = rate;
	deltaAngle = 2 * PI*(LFOfreq / sampleRate);

}

void Vasittu1AudioProcessor::releaseResources()
{
    // When playback stops, you can use this as an opportunity to free up any
    // spare memory, etc.
}

#ifndef JucePlugin_PreferredChannelConfigurations
bool Vasittu1AudioProcessor::isBusesLayoutSupported (const BusesLayout& layouts) const
{
  #if JucePlugin_IsMidiEffect
    ignoreUnused (layouts);
    return true;
  #else
    // This is the place where you check if the layout is supported.
    // In this template code we only support mono or stereo.
    if (layouts.getMainOutputChannelSet() != AudioChannelSet::mono()
     && layouts.getMainOutputChannelSet() != AudioChannelSet::stereo())
        return false;

    // This checks if the input layout matches the output layout
   #if ! JucePlugin_IsSynth
    if (layouts.getMainOutputChannelSet() != layouts.getMainInputChannelSet())
        return false;
   #endif

    return true;
  #endif
}
#endif

void Vasittu1AudioProcessor::processBlock (AudioSampleBuffer& buffer, MidiBuffer& midiMessages)
{
    ScopedNoDenormals noDenormals;
    const int totalNumInputChannels  = getTotalNumInputChannels();
    const int totalNumOutputChannels = getTotalNumOutputChannels();

  
    for (int i = totalNumInputChannels; i < totalNumOutputChannels; ++i)
        buffer.clear (i, 0, buffer.getNumSamples());


	auto depthCopy = round(depth*getSampleRate());
	auto delay = round(depth*getSampleRate());
	if (rate != LFOfreq) {
		LFOfreq = rate;
		deltaAngle = 2 * PI*(LFOfreq / getSampleRate());
	}

    // This is the place where you'd normally do the guts of your plugin's
    // audio processing...
    for (int channel = 0; channel < totalNumInputChannels; ++channel)
    {
		int pos = writeIndex[channel];
        float* channelData = buffer.getWritePointer (channel);
	
		
		
		// Saturation
		auto dist = 0.9*Saturation_gain;
		auto Q = Saturation_threshold;
		//auto g = 0.3*Saturation_gain;
		auto g = 1;


		auto volume = 3;
		//depth = 0.01;
		//rate = 2;


		// Main process loop
		for (int n = 0; n < buffer.getNumSamples(); ++n) {

			const float x = channelData[n];
			
			// Vibrato
			vBuffer[channel][pos] = channelData[n];
			double modfreq = sin(phase[channel]);
			phase[channel] = phase[channel] + deltaAngle;

			if (phase[channel] > PI * 2) {
				phase[channel] = phase[channel] - (2 * PI);
			}
			
			float tap = 1 + delay + depthCopy * modfreq;
			int n2 = floor(tap);
			float frac = tap - n2;
			int rindex = floor(pos - n2);
			if (rindex < 0) {
				rindex = rindex + lenBuffer;
			}

			float sample = 0;
			if (rindex == 0) {
				sample += vBuffer[channel][lenBuffer - 1] * frac + (1 - frac)*vBuffer[channel][rindex];
			}
			else {
				sample += vBuffer[channel][rindex - 1] * frac + (1 - frac)*vBuffer[channel][rindex];
			}
			pos = (pos + 1) % lenBuffer;

			if (rate != 0) {
				channelData[n] = 0.7*channelData[n] + 0.3*sample;
			}
			
			// Delay




			// Saturation
			if (Effect) {
				channelData[n] = processDistortion(channelData[n], g, Q, dist, getSampleRate(), channel);
			}
			
			// 3-point equalizer
			if (EQ) {
				// Bass (Low-shelving)
				for (int i = 0; i < low_filters.size(); ++i) {
					low_filters[i]->setCoefficients(IIRCoefficients::makeLowShelf(getSampleRate(), Fc_low, Q_low, Gain_bass));
				}

				// Do the filtering
				float filtered = low_filters[channel]->processSingleSampleRaw(x);

				// Mid (Peaking)
				for (int i = 0; i < mid_filters.size(); ++i) {
					mid_filters[i]->setCoefficients(IIRCoefficients::makePeakFilter(getSampleRate(), Fc_mid, Q_mid, Gain_mid));
				}

				// Do the filtering
				float filtered2 = mid_filters[channel]->processSingleSampleRaw(filtered);


				// Treble (High-shelving)
				for (int i = 0; i < high_filters.size(); ++i) {
					high_filters[i]->setCoefficients(IIRCoefficients::makeHighShelf(getSampleRate(), Fc_high, Q_high, Gain_treble));
				}

				// Do the filtering
				float filtered3 = high_filters[channel]->processSingleSampleRaw(filtered2);
				
				// Output
				channelData[n] = filtered3;
			}
			

		}
	
		writeIndex[channel] = pos;
		
    }

	// Output Gain
	buffer.applyGain(Output_Gain);


	
}




//==============================================================================
float Vasittu1AudioProcessor::processDistortion2(float sample, int channel) {
	// Soft clipping distortion based on quadratic equation

	float th1 = 1.0f / 3.0f, th2 = 2.0f / 3.0f;
	float out;

	if (sample > th2) {
		out = 1.0f;
	}
	else if (sample > th1) {
		out = (3.0f - (2.0f - 3.0f*sample)*(2.0f - 3.0f*sample)) / 3.0f;
	}
	else if (sample < -th2) {
		out = -1.0f;
	}
	else {
		out = 2.0f*sample;
	}

	return out;
}

float Vasittu1AudioProcessor::processDistortion(float sample, double g, double Q, double dist, double fs, int channel) {
	// Tube Amp distortion based on: M. Z�lzer, DAFX Book, Chapt. 4. 
	
	float out;
	
	// Apply gain
	sample *= g;

	// The distortion algorithm, assymetrical function
	if (Q == 0)
	{
		if (sample == Q) {
			out = 1 / dist;
		} else {
			out = sample / (1 - exp(-dist * sample));
		}
	} else {
		if (sample == Q) {
			out = 1 / dist + Q / (1 - exp(dist*Q));
		} else {
			out = (sample - Q) / (1 - exp(-dist * (sample - Q))) + Q / (1 - exp(dist*Q));
		}
	}

	
	// Additional filtering to simulate characteristics of a Tube Amp

	/*
	for (int i = 0; i < saturation_lows.size(); ++i) {
		saturation_lows[i]->setCoefficients(IIRCoefficients::makeLowShelf(getSampleRate(), Fc_low, Q_low, 1.3));
	}
	*/

	for (int i = 0; i < saturation_mids.size(); ++i) {
		saturation_mids[i]->setCoefficients(IIRCoefficients::makePeakFilter(getSampleRate(), 500, 0.5, 1.8)); // Mid Boost
	}

	/*
	for (int i = 0; i < saturation_highs.size(); ++i) {
		saturation_highs[i]->setCoefficients(IIRCoefficients::makeLowShelf(getSampleRate(), 10000, Q_high, 0.7));
	}
	*/



	// Do the filtering
	//float filtered = saturation_lows[channel]->processSingleSampleRaw(out);
	float filtered2 = saturation_mids[channel]->processSingleSampleRaw(out);
	//float filtered3 = saturation_highs[channel]->processSingleSampleRaw(filtered2);

	return filtered2;
}




//==============================================================================
bool Vasittu1AudioProcessor::hasEditor() const
{
    return true; // (change this to false if you choose to not supply an editor)
}

AudioProcessorEditor* Vasittu1AudioProcessor::createEditor()
{
    return new PluginEditor (*this);
}

//==============================================================================
void Vasittu1AudioProcessor::getStateInformation (MemoryBlock& destData)
{
    // You should use this method to store your parameters in the memory block.
    // You could do that either as raw data, or use the XML or ValueTree classes
    // as intermediaries to make it easy to save and load complex data.
}

void Vasittu1AudioProcessor::setStateInformation (const void* data, int sizeInBytes)
{
    // You should use this method to restore your parameters from this memory block,
    // whose contents will have been created by the getStateInformation() call.
}

//==============================================================================
// This creates new instances of the plugin..
AudioProcessor* JUCE_CALLTYPE createPluginFilter()
{
    return new Vasittu1AudioProcessor();
}
